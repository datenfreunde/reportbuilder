resource "aws_iam_access_key" "pollyuser" {
  user    = "${aws_iam_user.pollyuser.name}"
  pgp_key = "keybase:mvtango"
}

resource "aws_iam_user" "pollyuser" {
  name = "pollyuser"
  path = "/system/"
}

resource "aws_iam_user_policy" "access_polly" {
  name = "access_polly"
  user = "${aws_iam_user.pollyuser.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "polly:SynthesizeSpeech",
        "polly:DescribeVoices"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}



output "credentials" {
  value = "echo export AWS_ACCESS_KEY_ID=${aws_iam_access_key.pollyuser.id};echo export AWS_SECRET_ACCESS_KEY=`echo ${aws_iam_access_key.pollyuser.encrypted_secret} |  base64 --decode | keybase pgp decrypt`"
}




