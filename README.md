# Reportbuilder

Dockerized SSML Template Authoring Tool using AWS Polly. 

It also contains a small application that will play a preconfigured message 
if it detects a change in an arbitrary JSON document on the Internet.


## Running the authoring tool

`make build` - will build a docker container.

`make run`   - run the container 

See Makefile for hints on how this is done. You need credentials from Amazon 
to run the tool, see config.makefile-example for instructions.


## Playing with Polly 

The docker container exposes a web authoring application where you can edit template and data, and listen to the result. 


## Getting speech mp3s programmatically

You can use this as a simple API: /read?voiceid=...&text=....&data=... &outputFormat=mp3


/read?voiceId=Hans&text=Der%20Wasserz%C3%A4hler%20steht%20auf%20%7B%7B%20data.water%20%7D%7D.%20%0ADer%20Verbrauch%20innerhalb%20der%20letzten%2012%20Stunden%20betrug%0A%7B%25%20if%20data.delta%20%7C%20int%20%3E%2012%20%25%7D%20leider%20%7B%25%20endif%20%25%7D%20%7B%7B%20data.delta%20%7D%7D%20Kubik%20Meter.%20%0A%0ADiese%20Nachricht%20ist%20von%20%7B%7B%20now.strftime(%22%25H%22)%20%7C%20int%20%2B%202%20%7D%7D%20Uhr%20%7B%7B%20now.strftime(%22%25M%22)%20%7C%20int%20%7D%7D.&data=%7B%20%22water%22%20%3A%20%221245%2C6%22%2C%20%22delta%22%20%3A%20%2299%22%20%7D&outputFormat=mp3

voiceId:Hans
text:Der Wasserzähler steht auf {{ data.water }}. 
Der Verbrauch innerhalb der letzten 12 Stunden betrug
{% if data.delta | int > 12 %} leider {% endif %} {{ data.delta }} Kubik Meter. 

Diese Nachricht ist von {{ now.strftime("%H") | int + 2 }} Uhr {{ now.strftime("%M") | int }}.
data:{ "water" : "1245,6", "delta" : "99" }
outputFormat:mp3


## Generate mp3 and SSML on Change Detection

`make runner` 

will build a new mp3 and SSML file using the included `src/templates/zaehler.txt` every time
the document found at the preconfigured URL in `src/runner.sh` changes. The files will reside
in /tmp inside the container - if you want to use them outside, export this directory to the
host.


## Play Audio on Change Detection

`make runner-local` 

This will need a local Python3.6 and `mpg123` installed locally. 

## Generate credentials

If you use `terraform` (recommended) you can use `terraform apply` in this directory to generate credentials
for AWS that allow speech generation and nothing else.  The secret access key ID will be 
encrypted with the PGP key configured in `terraform/polly_user.tf`.





## Credits
This work is part of the [Smartorchestra](http://smartorchestra.de/) project, co-financed by the [Federal Ministery of Economics and Technology](https://www.bmwi.de/).

