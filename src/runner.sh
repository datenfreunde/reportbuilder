#! /bin/bash

# {"water":"1000","delta":"47"}


SOURCE=${SOURCE:-http://81.14.203.235:1026/v2/entities/e2watch-report/attrs/report/value} 

DEST=/tmp/report.json
AUDIOFILE=/tmp/message.mp3
DIR=$(dirname $0)/
VOCIEID=${VOICEID:-Marlene}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:-eu-central-1}

export AWS_DEFAULT_REGION

PAUSE=${PAUSE:-2} # Seconds between runs 


while /bin/true ; do

	curl -s -H 'Accept: application/json' \
            -H 'Fiware-Service: fokuscustomer' \
            ${SOURCE} >current.json
	# curl -s -H 'Accept: application/json' -H 'Fiware-Service: fokuscustomer' http://81.14.203.235:1026/v2/entities/e2watch-hist/attrs/number/value >current.json
	if diff current.json last.json ; then 

	  echo $(cat last.json) is $(cat current.json) 

	else

	  echo $(cat last.json) is not $(cat current.json)
	  python ${DIR}cli.py --voiceId=${VOICEID} \
                          --text=${DIR}templates/zaehler.txt \
                          --data=current.json \
			      --output=${AUDIOFILE} \
			      >${DEST}

	  printf "\nGenerated:"
	  cat $DEST
	  printf "\n\n"
	  cp current.json last.json
      which mpg123 && test -f ${AUDIOFILE} && mpg123 ${AUDIOFILE} 
	fi 

	sleep $PAUSE 

done
