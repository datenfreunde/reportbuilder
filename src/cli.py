from pollyhelper import synthesize, polly
import fire
import sys
import logging
import json
import os

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stderr,level=logging.DEBUG)


def run(voiceId="Marlene",data=None,text='',output=None) :
    if type(data) == str and os.path.exists(data) :
        logger.info(f"reading data from {data}")
        data=json.load(open(data))
    if type(text) == str and os.path.exists(text) :
        logger.info(f"reading text from {text}")
        text="".join(open(text).readlines())
    response = synthesize(voiceId, text, data, 'mp3')
    if output is not None and "AudioStream" in response:
        with open(output,"wb") as out :
            out.write(response['AudioStream'].read())
    sys.stdout.write(json.dumps(dict(ssml=response['text'])))


if __name__ == "__main__" :
    fire.Fire(run)
