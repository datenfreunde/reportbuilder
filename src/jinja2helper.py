
from jinja2 import Environment,PackageLoader,FunctionLoader
from functools import lru_cache

def fileloader(filename) :
    with open(filename) as f :
        return "".join((a for a in f.read()))



env = Environment(loader=FunctionLoader(fileloader),
                  extensions=[]
                  )

@lru_cache(maxsize=128)
def mktemplate(s) :
    return env.from_string(s)


def j2fill(t,**kwargs) :
	return mktemplate(t).render(**kwargs)


