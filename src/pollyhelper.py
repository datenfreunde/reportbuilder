import logging
from jinja2helper import j2fill
import os
import json
import sys
import datetime

logger = logging.getLogger(__name__)

from boto3 import Session

# Create a client using AWS_PROFILE
# pr AWS_ACCESS_KEY_ID / AWS_SECRET_ACCESS_KEY
if "AWS_PROFILE" in os.environ :
    session = Session(profile_name=os.environ.get("AWS_PROFILE"))
elif "AWS_ACCESS_KEY_ID" in os.environ :
    session = Session(aws_access_key_id=os.environ["AWS_ACCESS_KEY_ID"],
                      aws_secret_access_key=os.environ["AWS_SECRET_ACCESS_KEY"])
    logger.debug("credentials: SECRET {}...{}, KEY {AWS_ACCESS_KEY_ID}".format(os.environ["AWS_SECRET_ACCESS_KEY"][0],os.environ["AWS_SECRET_ACCESS_KEY"][-1],**os.environ))
else :
    logger.error("Please set AWS_SECRET_ACCESS_KEY and AWS_ACCESS_KEY_ID or AWS_PROFILE to provide the server with credentials")
    sys.exit(1)


polly = session.client("polly")


def synthesize(voiceId,text,data,outputFormat) :
    logger.debug("Data was {}".format(repr(data)))
    try :
            data = json.loads(data)
    except Exception as e:
            logger.debug(f"JSON Decoding error: {e}")

    if type(data) != dict :
        data=dict(value=data)

    logger.debug("Data is {}".format(repr(data)))
    if data is not None :
            text = j2fill(text,now=datetime.datetime.now(),data=data)
            logger.debug("rendered text is: {}".format(repr(text)))


    # Validate the parameters, set error flag in case of unexpected
    # values
    if len(text) == 0:
        raise ValueError("No Text")
    else:
        if voiceId :
            # Request speech synthesis
            if text.find("<")>-1 :
                ttype="ssml"
            else :
                ttype="text"
            response = polly.synthesize_speech(Text=text,
                                                TextType=ttype,
                                                VoiceId=voiceId,
                                                OutputFormat=outputFormat)
        else :
            ttype=""
            response = dict()
        response["text"]=text
        logger.info("Requested {ttype} {voiceId}: {text}".format(**locals()))
        return response
