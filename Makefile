SHELL := /bin/bash

PARAM = --host=192.168.56.101
CONTAINERNAME = pollyserver


NETWORK := local

PWD := $(shell pwd)

network: 
	test $(NETWORK) = "$$(docker network ls --filter name=$(NETWORK) --format '{{.Name}}')" || docker network create $(NETWORK)



include config.makefile

server:
	# AWS_PROFILE=odc AWS_DEFAULT_REGION=eu-west-1 python server.py $(PARAM)
	python3 server.py $(PARAM)

build:
	docker build . -t $(CONTAINERNAME)



PORT=8000
MODE=-it
run: network 
	echo PORT=$(PORT) -- port to expose  MODE=$(MODE) -- can be -it or -d ;\
	docker run --rm  -e AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
		             -e AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
			      -e AWS_DEFAULT_REGION=eu-central-1 \
			     --network=$(NETWORK) \
			     --network-alias=$(CONTAINERNAME) \
			     -p $(PORT):8000 $(MODE) $(CONTAINERNAME)
CLI := -- --help
cli:
	@docker run --rm  -e AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
		             -e AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
			     -e AWS_DEFAULT_REGION=eu-central-1 \
			     --network=$(NETWORK) \
			     --network-alias=$(CONTAINERNAME) \
			     -it $(CONTAINERNAME) python ./cli.py $(CLI)

VOICEID := Hans 
runner:
	@docker run --rm  -e AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
		             -e AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
			     -e AWS_DEFAULT_REGION=eu-central-1 \
				 -e VOICEID=$(VOICEID) \
			     -v $(PWD)/../nginx/html/e2watchreport:/tmp \
			     --name runner \
			     -it $(CONTAINERNAME) /bin/bash ./runner.sh

install-local:
	@if python -V | grep '3\.6' >/dev/null ; then \
		pip install -r src/requirements.txt; \
	else \
		echo Your $$(which python) is $$(python -V). You need python 3.6 to run this locally ;\
	fi ; \
	echo Testing availability of mpg123 for audio reproduction ;\
	which mpg123 || sudo apt-get install mpg123

runner-local:
	@VOICEID=$(VOICEID) AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
	./src/runner.sh

images: dpa-newslab-logo.dataurl polly.dataurl

%.dataurl : %.png
	uuencode -m $< X | sed '1d;$d' | tr -d '\n' | sed -r 's_([^=]*).*_"data:image/png;base64,\1"_' >$@

show-credentials:
	AWS_PROFILE=myaws AWS_DEFAULT_REGION=eu-central-1 terraform output -state=terraform/terraform.tfstate credentials | bash

WATER :=350
DELTA := 10 
set-values:
	 curl -X PUT -H "Fiware-Service: fokuscustomer" -H "Content-Type: application/json" -d '{"water": "$(WATER)","delta": "$(DELTA)"}' http://81.14.203.235:1026/v2/entities/e2watch-report/attrs/report/value
