FROM python:3.6-alpine

RUN apk update && \
    apk upgrade && \
    apk add bash curl &&\
    pip --no-cache-dir install --upgrade pip &&\
    pip --no-cache-dir install boto3 


add [ "src/requirements.txt", "/"]

RUN pip --no-cache-dir install -r requirements.txt

HEALTHCHECK --interval=300s \
     CMD wget -T 3 --output-document=- 'http://localhost:8000/voices?healthcheck' >/dev/null 2>&1 || exit 1 

EXPOSE 8000

ADD [ "src/", "/" ]


CMD [ "python", "./server.py", "--host=0.0.0.0" ]

